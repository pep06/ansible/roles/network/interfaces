# Ansible role ``network/interfaces``

## Description
This role is a simple wrapper around more specific sub-roles. It is used
to delegate the configuration of network interfaces to a specialized
role fitting the platform of the configured host.

## Delegations
This role delegates network configuration to one of the following
provider roles, according to the value of the ``ansible_distribution``
variable on the configured host:

| ``ansible_distribution`` | Provider role                   |
| ------------------------ | ------------------------------- |
| ``FreeBSD``              | ``network/interfaces.freebsd``  |
| ``Debian``               | ``network/interfaces.ifupdown`` |